export const environment = {
  production: true,
  apiBaseUrl: 'http://server.backend.dev/',
  clientUrl: '?redirect_uri=http://client.frontend.dev/login'
};
